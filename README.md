# GPX Route Generator Web

Web interface for the [GPX Route Generator Core](https://gitlab.com/3nvy/gpx-route-generator-core) project. As a PWA, it will work offline once you access it once and you can install it as an app on a browser that allows it.

You can access the live version at https://gpx-route-generator-web.onrender.com/


# Run a Local Instance

**- Requires Node.js -**

Install Dependencies
<pre>
rm -rf node_modules/
npm i --legacy-peer-deps
</pre>

Run a Development Instance
<pre>
npm run dev
</pre>

Run a Production Instance
<pre>
npm run build && npm start
</pre>

Run a Production Instance Locally
<pre>
npm run build && npm start
npm install -g serve
serve -s build
</pre>