#!/bin/sh

# Export .env variables.
export $(grep -v '^#' .env | xargs)

# Rename precache file with app version.
mv ./build/asset-manifest.json "./build/asset-manifest_v${REACT_APP_VERSION}.json"

# Update cache version on Service Worker.
sed -i "s|CACHE_VERSION|$REACT_APP_VERSION|g" ./build/service-worker.js